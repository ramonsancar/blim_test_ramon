package com.veo.televisa.blimtestproject.listeners;


public interface EpisodeSelected {
    void onEpisodeSelected(int episodeId);
}
