package com.veo.televisa.blimtestproject.view.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.databinding.library.baseAdapters.BR;
import com.veo.televisa.blimtestproject.BlimApplication;
import com.veo.televisa.blimtestproject.R;
import com.veo.televisa.blimtestproject.listeners.EpisodeSelected;
import com.veo.televisa.blimtestproject.model.Episodes;
import com.veo.televisa.blimtestproject.model.Seasons;

import io.realm.RealmList;
import timber.log.Timber;

public class EpisodeRecyclerAdapter extends RecyclerView.Adapter<EpisodeViewHolder> {

    private final EpisodeSelected mListener;
    private RealmList<Episodes> mEpisodes;

    public EpisodeRecyclerAdapter(RealmList<Seasons> seasons, EpisodeSelected listener) {
        mListener = listener;
        if(seasons == null || seasons.size() == 0) {
            mEpisodes = new RealmList<Episodes>();
        }
        else {
            mEpisodes = seasons.get(0).getEpisodes();
        }
    }

    public void updateEpisodes(RealmList<Seasons> seasons) {
        if(seasons != null && seasons.size() > 0) {
            //check for modifications between seasons.get(0).getEpisodes() and mEpisodes
            if(seasons.get(0).getEpisodes().size() != mEpisodes.size()) {
                int modifiedCount = seasons.get(0).getEpisodes().size() - mEpisodes.size();
                mEpisodes = seasons.get(0).getEpisodes();
                if(modifiedCount > 0) {
                    notifyItemRangeInserted(mEpisodes.size(), modifiedCount);
                }
                else {
                    if(modifiedCount < 0) {
                        notifyItemRangeRemoved(seasons.get(0).getEpisodes().size(), modifiedCount * -1);
                    }
                    //do nothing on 0
                }
            }
        }
    }

    @Override
    public EpisodeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) BlimApplication.getAppContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.epidose_item, parent, false);
        return new EpisodeViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(EpisodeViewHolder holder, int position) {
        Episodes episode = mEpisodes.get(position);
        holder.getViewBinding().setVariable(BR.episodeAdapter, this);//bind this adapter for click events
        holder.getViewBinding().setVariable(BR.episode, episode);//bind episode to the episode layout
        holder.getViewBinding().getRoot().findViewById(R.id.episode_info).setTag(position);
        holder.getViewBinding().getRoot().findViewById(R.id.episode_play).setTag(episode.getId());
    }

    public void onClickItem(View view) {
        Timber.d("onClickItem on "+mEpisodes.get((Integer) view.getTag()).getTitleEditorial());

        view.setSelected(!view.isSelected());
        view.findViewById(R.id.episode_description).setVisibility(view.isSelected() ? View.VISIBLE : View.GONE);
    }

    public void onPlayEpisode(View view) {
        mListener.onEpisodeSelected((Integer) view.getTag());
    }

    @Override
    public int getItemCount() {
        return mEpisodes.size();
    }
}