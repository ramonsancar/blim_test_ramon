package com.veo.televisa.blimtestproject.model;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class People extends RealmObject{
    @PrimaryKey
    private int id;
    private String category;
    private String entity;
    private String displayName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}