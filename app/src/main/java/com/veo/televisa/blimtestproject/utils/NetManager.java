package com.veo.televisa.blimtestproject.utils;

import android.text.TextUtils;

import com.veo.televisa.blimtestproject.BlimApplication;
import com.veo.televisa.blimtestproject.R;
import com.veo.televisa.blimtestproject.model.Seasons;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import io.realm.RealmList;
import io.realm.RealmResults;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class NetManager {

    private static NetManager m_NetMInstance;

    public static NetManager getInstance() {
        if(m_NetMInstance == null) {
            m_NetMInstance = new NetManager();
        }
        return m_NetMInstance;
    }

    private NetManager() {
        Timber.d("NetManager constructor");
    }

    public Observable<RealmList<Seasons>> retrieveSerie(final int serieId) {
        return Observable.create(new Observable.OnSubscribe<String>() {
                @Override
                public void call(Subscriber<? super String> subscriber) {
                    Timber.d("retrieveSerie start loading");
                    subscriber.onNext(loadJSONFromAsset(serieId));
                    subscriber.onCompleted();
                }
            })
                .delay(2L, java.util.concurrent.TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .map(new Func1<String, Boolean>() {
                    @Override public Boolean call(String serieJson) {
                        Timber.d("we got an answer: %s", serieJson);
                        try {
                            if (!TextUtils.isEmpty(serieJson)) {
                                Timber.d("Saving serie");

                                JSONObject jsonObject = new JSONObject(serieJson);
                                DataManager.getInstance().saveSerie(jsonObject.getJSONObject("data").toString());
                                return true;
                            }
                        }catch(Exception e) {
                            Timber.d("Error Saving serie, return false %s", e.getMessage());
                        }
                        return false;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Func1<Boolean, RealmList<Seasons>>() {
                    @Override public RealmList<Seasons> call(Boolean valuesSaved) {
                        try {
                            if (valuesSaved) {
                                Timber.d("Reading recently saved values");
                                return DataManager.getInstance().getSeasonsFromSerieId(serieId);
                            }
                        }catch(Exception e) {}
                        return null;
                    }
                })
                ;
    }

    public String loadJSONFromAsset(int serieId) {
        String json = null;
        try {

            InputStream is = BlimApplication.getAppContext().getResources().openRawResource(R.raw.testasset);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
//            Timber.e("Error reading the JSON from res/raw/testasset: %s",ex.getMessage());
            return null;
        }
//        Timber.d("Json DATA: %s",json);
        return json;

    }
}
