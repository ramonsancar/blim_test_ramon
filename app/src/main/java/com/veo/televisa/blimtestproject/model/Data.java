package com.veo.televisa.blimtestproject.model;


import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Data extends RealmObject{

    @PrimaryKey
    private int id;
    private Genre genre;
    private ParentalRating parentalRating;
    private RealmList<People> people;
    private RealmList<Pictures> pictures;
    private RealmList<Seasons> seasons;
    private Studio studio;
    private String airDate;
    private String subCategory;
    private String synopsis;
    private String synopsisShort;
    private String title;
    private String titleEditorial;
    private String titleFirstLetter;
    private String titleSeo;
    private String titleShort;
    private float averageRating;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public ParentalRating getParentalRating() {
        return parentalRating;
    }

    public void setParentalRating(ParentalRating parentalRating) {
        this.parentalRating = parentalRating;
    }

    public RealmList<People> getPeople() {
        return people;
    }

    public void setPeople(RealmList<People> people) {
        this.people = people;
    }

    public RealmList<Pictures> getPictures() {
        return pictures;
    }

    public void setPictures(RealmList<Pictures> pictures) {
        this.pictures = pictures;
    }

    public RealmList<Seasons> getSeasons() {
        return seasons;
    }

    public void setSeasons(RealmList<Seasons> seasons) {
        this.seasons = seasons;
    }

    public Studio getStudio() {
        return studio;
    }

    public void setStudio(Studio studio) {
        this.studio = studio;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getSynopsisShort() {
        return synopsisShort;
    }

    public void setSynopsisShort(String synopsisShort) {
        this.synopsisShort = synopsisShort;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleEditorial() {
        return titleEditorial;
    }

    public void setTitleEditorial(String titleEditorial) {
        this.titleEditorial = titleEditorial;
    }

    public String getTitleFirstLetter() {
        return titleFirstLetter;
    }

    public void setTitleFirstLetter(String titleFirstLetter) {
        this.titleFirstLetter = titleFirstLetter;
    }

    public String getTitleSeo() {
        return titleSeo;
    }

    public void setTitleSeo(String titleSeo) {
        this.titleSeo = titleSeo;
    }

    public String getTitleShort() {
        return titleShort;
    }

    public void setTitleShort(String titleShort) {
        this.titleShort = titleShort;
    }

    public float getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(float averageRating) {
        this.averageRating = averageRating;
    }

    public String getAirDate() {
        return airDate;
    }

    public void setAirDate(String airDate) {
        this.airDate = airDate;
    }
}
