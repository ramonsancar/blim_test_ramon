package com.veo.televisa.blimtestproject.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Episodes extends RealmObject {

    @PrimaryKey
    private int id;
    private int duration;
    private int episodeNumber;
    private String synopsis;
    private String title;
    private String titleEditorial;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(int episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleEditorial() {
        return titleEditorial;
    }

    public void setTitleEditorial(String titleEditorial) {
        this.titleEditorial = titleEditorial;
    }
}
