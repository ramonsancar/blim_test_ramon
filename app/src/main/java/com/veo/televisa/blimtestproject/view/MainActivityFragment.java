package com.veo.televisa.blimtestproject.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.veo.televisa.blimtestproject.R;
import com.veo.televisa.blimtestproject.databinding.MainFragmentBinding;
import com.veo.televisa.blimtestproject.view.base_classes.BaseFragment;
import com.veo.televisa.blimtestproject.viewmodel.Lifecycle;
import com.veo.televisa.blimtestproject.viewmodel.MainActivityVM;
import com.veo.televisa.blimtestproject.viewmodel.MainActivityVMContract;

import timber.log.Timber;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends BaseFragment implements MainActivityVMContract.View {

    private MainActivityVM mainActivityVM;

    public MainActivityFragment() {
//        loadJSONFromAsset();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivityVM = new MainActivityVM();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        loadJSONFromAsset();
        MainFragmentBinding fragmentMainBinding = DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false);
        fragmentMainBinding.setMainFragmentVM(mainActivityVM);

        return fragmentMainBinding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!isInit) {
            mainActivityVM.clearOldData();
        }
        isInit = true;
    }

    @Override
    protected Lifecycle.ViewModel getViewModel() {
        return mainActivityVM;
    }

    @Override
    protected Lifecycle.View getLifecycleView() {
        return this;
    }

    @Override
    protected boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finishActivity();
            return true;
        }
        return false;
    }

    @Override
    public void onSerieSelected(int serieId) {
        Timber.d("User selected serie with ID: %d", serieId);
        Bundle extras = new Bundle();
        extras.putInt("serieId", serieId);
        launchActivity(AssetDetailActivity.class, extras);
        finishActivity();
    }


    //move this method to NetManager, faking a network call to retrieve the JSON
//    public String loadJSONFromAsset() {
//        String json = null;
//        try {
//
//            InputStream is = getActivity().getResources().openRawResource(R.raw.testasset);
//
//            int size = is.available();
//
//            byte[] buffer = new byte[size];
//
//            is.read(buffer);
//
//            is.close();
//
//            json = new String(buffer, "UTF-8");
//
//
//        } catch (IOException ex) {
//            ex.printStackTrace();
//            return null;
//        }
//
//        Log.d("Json DATA",json);
//        return json;
//
//    }
}
