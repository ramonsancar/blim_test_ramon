package com.veo.televisa.blimtestproject.binding;

import android.databinding.BindingAdapter;
import android.graphics.Picture;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.veo.televisa.blimtestproject.BlimApplication;
import com.veo.televisa.blimtestproject.R;
import com.veo.televisa.blimtestproject.model.Genre;
import com.veo.televisa.blimtestproject.model.ParentalRating;
import com.veo.televisa.blimtestproject.model.Pictures;
import com.veo.televisa.blimtestproject.model.Seasons;

import io.realm.RealmList;

public class Adapters {

    @BindingAdapter({"setVisibility"})
    public static void setVisibility(View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter({"setDuration"})
    public static void setDuration(TextView view, int duration) {
        duration = duration / 60;
        view.setText(duration + " m");
    }

    @BindingAdapter({"setParentalRating"})
    public static void setParentalRating(TextView view, ParentalRating parentalRating) {
        if(parentalRating!= null) {
            view.setText(parentalRating.getTitle());
        }
    }

    @BindingAdapter({"setGenre"})
    public static void setGenre(TextView view, Genre genre) {
        if(genre!= null) {
            view.setText(genre.getTitle());
        }
    }

    @BindingAdapter({"setAirDate"})
    public static void setAirDate(TextView view, String airdate) {
        if(airdate != null) {
            view.setText(airdate.substring(0, 4));
        }
    }

    @BindingAdapter({"loadImage"})
    public static void loadLandscapeImage(ImageView view, String url) {
        if(!TextUtils.isEmpty(url)) {
            Picasso.with(view.getContext()).load(url).into(view);
        }
    }

    @BindingAdapter({"loadLandscapeImage"})
    public static void loadLandscapeImage(ImageView view, RealmList<Pictures> pictures) {
        if(pictures != null) {
            for (Pictures picture : pictures) {
                //get only the landscape image
                if (picture.getPictureType().equalsIgnoreCase("landscape")) {
                    //weird format for the images, use the second one (smaller)

                    Picasso.with(view.getContext()).load(picture.getVersions().get(1).getLink()).into(view);
                }
            }
        }
    }

    @BindingAdapter({"setSeasons"})
    public static void setSeasons(TextView view, RealmList<Seasons> seasons) {
        if(seasons != null) {
            view.setText(seasons.size() + " " + (seasons.size() == 1 ? BlimApplication.getAppContext().getString(R.string.seasons_singular) : BlimApplication.getAppContext().getString(R.string.seasons_plural)));
        }
    }
}
