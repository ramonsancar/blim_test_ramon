package com.veo.televisa.blimtestproject.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.veo.televisa.blimtestproject.R;
import com.veo.televisa.blimtestproject.databinding.AssetDetailFragmentBinding;
import com.veo.televisa.blimtestproject.utils.DataManager;
import com.veo.televisa.blimtestproject.view.base_classes.BaseFragment;
import com.veo.televisa.blimtestproject.viewmodel.AssetDetailsVM;
import com.veo.televisa.blimtestproject.viewmodel.AssetDetailsVMContract;
import com.veo.televisa.blimtestproject.viewmodel.Lifecycle;

import timber.log.Timber;


///**
// * A simple {@link Fragment} subclass.
// * Activities that contain this fragment must implement the
// * {@link AssetDetailFragment.OnFragmentInteractionListener} interface
// * to handle interaction events.
// * Use the {@link AssetDetailFragment#newInstance} factory method to
// * create an instance of this fragment.
// */
public class AssetDetailFragment extends BaseFragment implements AssetDetailsVMContract.View{
//    private OnFragmentInteractionListener mListener;
    private int serieId;
    private AssetDetailsVM assetDetailsVM;
    private RecyclerView recyclerView;
    private AssetDetailFragmentBinding assetDetailFragmentBinding;
    private String STATE = "STATE_KEY";
    private Parcelable layoutManagerState;
//    // TODO: Rename and change types and number of parameters
//    public static AssetDetailFragment newInstance() {
//        AssetDetailFragment fragment = new AssetDetailFragment();
//        Bundle args = new Bundle();
//        fragment.setArguments(args);
//        return fragment;
//    }

    public AssetDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            serieId = getArguments().getInt("serieId",-1); //-1 should not be received
        }
        assetDetailsVM = new AssetDetailsVM();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        assetDetailFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.asset_detail_fragment, container, false);
        assetDetailFragmentBinding.setAssetDetailsVM(assetDetailsVM);
        return assetDetailFragmentBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!isInit) {
            assetDetailsVM.loadSerie(serieId, assetDetailFragmentBinding);
        }
        isInit = true;
    }

    @Override
    protected Lifecycle.ViewModel getViewModel() {
        return assetDetailsVM;
    }

    @Override
    protected Lifecycle.View getLifecycleView() {
        return this;
    }

    @Override
    protected boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            DataManager.getInstance().destroy();
            finishActivity();
            return true;
        }
        return false;
    }

    @Override
    public void onEpisodeSelected(int episodeId) {
        Toast.makeText(getActivity(), "PLAY EPISODE WITH ID "+episodeId, Toast.LENGTH_SHORT ).show();
    }

//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }


//    /**
//     * This interface must be implemented by activities that contain this
//     * fragment to allow an interaction in this fragment to be communicated
//     * to the activity and potentially other fragments contained in that
//     * activity.
//     * <p/>
//     * See the Android Training lesson <a href=
//     * "http://developer.android.com/training/basics/fragments/communicating.html"
//     * >Communicating with Other Fragments</a> for more information.
//     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        public void onFragmentInteraction(Uri uri);
//    }

}
