package com.veo.televisa.blimtestproject.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ParentalRating extends RealmObject{
    @PrimaryKey
    private int id;
    private String title;

    private String permittedRating;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPermittedRating() {
        return permittedRating;
    }

    public void setPermittedRating(String permittedRating) {
        this.permittedRating = permittedRating;
    }

}
