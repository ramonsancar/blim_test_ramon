package com.veo.televisa.blimtestproject.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Pictures extends RealmObject {

    @PrimaryKey
    private int id;
    private String entity;
    private String pictureType;
    private String title;
    private RealmList<PictureVersions> versions;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getPictureType() {
        return pictureType;
    }

    public void setPictureType(String pictureType) {
        this.pictureType = pictureType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public RealmList<PictureVersions> getVersions() {
        return versions;
    }

    public void setVersions(RealmList<PictureVersions> versions) {
        this.versions = versions;
    }
}
