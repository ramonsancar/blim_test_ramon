package com.veo.televisa.blimtestproject.model;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Studio extends RealmObject{

    @PrimaryKey
    private int id;
    private String entity;
    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}