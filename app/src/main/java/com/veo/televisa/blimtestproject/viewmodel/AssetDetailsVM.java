package com.veo.televisa.blimtestproject.viewmodel;

import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.veo.televisa.blimtestproject.BlimApplication;
import com.veo.televisa.blimtestproject.databinding.AssetDetailFragmentBinding;
import com.veo.televisa.blimtestproject.model.Data;
import com.veo.televisa.blimtestproject.model.Seasons;
import com.veo.televisa.blimtestproject.utils.DataManager;
import com.veo.televisa.blimtestproject.utils.NetManager;
import com.veo.televisa.blimtestproject.view.adapters.EpisodeRecyclerAdapter;
import com.veo.televisa.blimtestproject.view.adapters.GalleryAdapter;
import com.veo.televisa.blimtestproject.listeners.EpisodeSelected;
import io.realm.RealmList;
import rx.Observable;
import rx.Subscriber;
import timber.log.Timber;

public class AssetDetailsVM implements AssetDetailsVMContract.ViewModel, EpisodeSelected{

    private AssetDetailsVMContract.View viewCallback;
    private EpisodeRecyclerAdapter episodeAdapter;
    private AssetDetailFragmentBinding mAssetDetailFragmentBinding;
    public ObservableBoolean isLoading = new ObservableBoolean(false);
    private GalleryAdapter galleryAdapter;

    public AssetDetailsVM() {

    }

    @Override
    public void onViewResumed() {

    }

    @Override
    public void onViewAttached(@NonNull Lifecycle.View viewCallback) {
        this.viewCallback = (AssetDetailsVMContract.View) viewCallback;
    }

    @Override
    public void onViewDetached() {
        this.viewCallback = null;
    }

    @Override
    public void loadSerie(final int serieId, AssetDetailFragmentBinding assetDetailFragmentBinding) {

        //load the objects from the DB
                // if not present start a loading with a progressbar (isLoading = true)
                // if data is already present, load it into the view and check for changes in background (in this case the whole JSON is retrieved again, an optimized request that just receives the modifications is optimal

        mAssetDetailFragmentBinding = assetDetailFragmentBinding;

        Observable<RealmList<Seasons>> serieObservable = NetManager.getInstance().retrieveSerie(serieId);// observable that will retrieve the JSON

        Data serie = DataManager.getInstance().getSerie(serieId);
        if(serie != null && serie.getSeasons().size() > 0) {
            serieObservable = serieObservable.mergeWith(Observable.just(serie.getSeasons()));
            Timber.d("Offline check of serie and we have values!!!");
            mAssetDetailFragmentBinding.setSerie(serie);
            isLoading.set(false);
        }
        else {
            Timber.d("Offline check of serie and we are empty");
            isLoading.set(true);
        }

        RecyclerView recyclerView = mAssetDetailFragmentBinding.recyclerviewHolder.recyclerview;//findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
        episodeAdapter = new EpisodeRecyclerAdapter(serie == null ? null : serie.getSeasons(), this);
        recyclerView.setAdapter(episodeAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(BlimApplication.getAppContext(), DividerItemDecoration.VERTICAL));


        galleryAdapter = new GalleryAdapter(serie == null ? null : serie.getPictures().get(1).getVersions()); //1 is portrait
        ViewPager mGallery = mAssetDetailFragmentBinding.episodeGallery.gallery;
        mGallery.setAdapter(galleryAdapter);


        serieObservable.subscribe(new Subscriber<RealmList<Seasons>>() {
            @Override
            public void onCompleted() {
                Timber.d("retrieving serie completed");
            }

            @Override
            public void onError(Throwable e) {
                Timber.d("onError retrieving serie "+e.getMessage());
                //TODO add error message here
            }

            @Override
            public void onNext(RealmList<Seasons> seasons) {
                //onNext will run in the main thread
//                Timber.d("Serie with Id %d already have data", serieId);
//                for(Seasons season : seasons) {
//                    Timber.d("Season title %s", season.getTitle());
//                    for(Episodes episode : season.getEpisodes()) {
//                        Timber.d("   Episode title %s - %s", episode.getTitle(), episode.getTitleEditorial());
//                    }
//                }
                Data serieData = DataManager.getInstance().getSerie(serieId);
                mAssetDetailFragmentBinding.setSerie(serieData);
                episodeAdapter.updateEpisodes(seasons);
                if(serieData != null) {
                    galleryAdapter.updatePictures(serieData.getPictures().get(1).getVersions());
                }
                isLoading.set(false);
            }
        });
    }

    @Override
    public void onEpisodeSelected(int episodeId) {
        viewCallback.onEpisodeSelected(episodeId);
    }
}