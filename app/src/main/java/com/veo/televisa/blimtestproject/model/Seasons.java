package com.veo.televisa.blimtestproject.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Seasons extends RealmObject {

    @PrimaryKey
    private int id;
    private int number;
    private String title;
    private String entity;
    private int episodeCount;
    private RealmList<Episodes> episodes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public int getEpisodeCount() {
        return episodeCount;
    }

    public void setEpisodeCount(int episodeCount) {
        this.episodeCount = episodeCount;
    }

    public RealmList<Episodes> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(RealmList<Episodes> episodes) {
        this.episodes = episodes;
    }
}
