package com.veo.televisa.blimtestproject.viewmodel;

public interface MainActivityVMContract {
    //this interface will have methods that the ViewModel will call from the Activity/Fragment to update it's view
    interface View extends Lifecycle.View {
        void onSerieSelected(int serieId);//call this method when a serie is selected from the list
    }

    //this interface will have methods that the activity/fragment will call from the ViewModel
    interface ViewModel extends Lifecycle.ViewModel {
        void clearOldData();//call that could help to reduce the app's size. When the user navigates to a serie the adta is saved and the app's size will increase.

        void onStartPressed(android.view.View view);//receive button events
    }
}
