package com.veo.televisa.blimtestproject.view.base_classes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;

import com.veo.televisa.blimtestproject.R;
import com.veo.televisa.blimtestproject.viewmodel.Lifecycle;

public abstract class BaseFragment extends Fragment {

    //force sub-fragments to implement the getViewModel
    protected abstract Lifecycle.ViewModel getViewModel();
    //force sub-fragments to implement the getFragmentInstance to pass to onViewAttached
    protected abstract Lifecycle.View getLifecycleView();
    //force sub-fragments to implement the onKeyDown because the Activity will send this event to the corresponding fragment
    protected abstract boolean onKeyDown(int keyCode, KeyEvent event);

    protected boolean isInit = false;

    @Override
    public void onResume() {
        super.onResume();
        getViewModel().onViewResumed(); // resume ViewModel
    }

    @Override
    public void onStart() {
        super.onStart();
        getViewModel().onViewAttached(getLifecycleView());
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getActivity().isFinishing()) {
            getViewModel().onViewDetached();
        }
    }

    //generic methods to launch a new activity
    public void launchActivity(Class classToLaunch) {
        Intent launchIntent = new Intent(getActivity(), classToLaunch);
        startActivity(launchIntent);
        getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }
    public void launchActivity(Class classToLaunch, Bundle extras) {
        Intent launchIntent = new Intent(getActivity(), classToLaunch);
        launchIntent.putExtras(extras);
        startActivity(launchIntent);
        getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    //generic method to finish an activity
    public void finishActivity() {
        getActivity().finish();
        getActivity().overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

}
