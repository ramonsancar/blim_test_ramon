package com.veo.televisa.blimtestproject.viewmodel;

import android.support.annotation.NonNull;
import android.view.View;

import com.veo.televisa.blimtestproject.utils.NetManager;

import timber.log.Timber;

public class MainActivityVM implements MainActivityVMContract.ViewModel{

    private MainActivityVMContract.View viewCallback;

    public MainActivityVM() {

    }

    @Override
    public void onViewResumed() {

    }

    @Override
    public void onViewAttached(@NonNull Lifecycle.View viewCallback) {
        this.viewCallback = (MainActivityVMContract.View) viewCallback;
    }

    @Override
    public void onViewDetached() {
        this.viewCallback = null;
    }

    @Override
    public void clearOldData() {
        //clear old data here from Realm to reduce the app's size
        //just wait for the user to press on a serie
    }

    @Override
    public void onStartPressed(View view) {
        viewCallback.onSerieSelected(4158);// dummy call to launch series activity
    }
}