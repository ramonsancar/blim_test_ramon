package com.veo.televisa.blimtestproject.view.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.databinding.library.baseAdapters.BR;
import com.veo.televisa.blimtestproject.BlimApplication;
import com.veo.televisa.blimtestproject.R;
import com.veo.televisa.blimtestproject.databinding.ImageItemBinding;
import com.veo.televisa.blimtestproject.model.Episodes;
import com.veo.televisa.blimtestproject.model.PictureVersions;

import io.realm.RealmList;

public class GalleryAdapter extends PagerAdapter {

    private final LayoutInflater mLayoutInflater;
    private RealmList<PictureVersions> mPictures;

    public GalleryAdapter(RealmList<PictureVersions> pictures) {
        if(pictures == null || pictures.size() == 0) {
            mPictures = new RealmList<PictureVersions>();
        }
        else {
            mPictures = pictures;
        }
        mLayoutInflater = (LayoutInflater) BlimApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mPictures.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageItemBinding binding = DataBindingUtil.inflate(mLayoutInflater, R.layout.image_item, container, false);
        binding.setVariable(BR.picture, mPictures.get(position));
        binding.getRoot().setTag(position);
        container.addView(binding.getRoot());
        return binding.getRoot();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);
    }

    public void updatePictures(RealmList<PictureVersions> versions) {
        mPictures = versions;
        notifyDataSetChanged();
    }
}