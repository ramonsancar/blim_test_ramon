package com.veo.televisa.blimtestproject.viewmodel;

import android.support.v7.widget.RecyclerView;

import com.veo.televisa.blimtestproject.databinding.AssetDetailFragmentBinding;

public interface AssetDetailsVMContract {
    //this interface will have methods that the ViewModel will call from the Activity/Fragment to update it's view
    interface View extends Lifecycle.View {
        void onEpisodeSelected(int episodeId);//call this method when a episode is selected from the list
    }

    //this interface will have methods that the activity/fragment will call from the ViewModel
    interface ViewModel extends Lifecycle.ViewModel {
        void loadSerie(int serieId, AssetDetailFragmentBinding assetDetailFragmentBinding);//load the serie info from network

//        void onStartPressed(android.view.View view);//receive button events
    }
}
