package com.veo.televisa.blimtestproject.utils;

import com.veo.televisa.blimtestproject.model.Data;
import com.veo.televisa.blimtestproject.model.Seasons;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import timber.log.Timber;

public class DataManager {

    private static DataManager m_DataMInstance;
    private Realm realm;

    public static DataManager getInstance() {
        if(m_DataMInstance == null) {
            m_DataMInstance = new DataManager();
        }
        return m_DataMInstance;
    }

    private DataManager() {
        Timber.d("DataManager constructor");
        realm = Realm.getDefaultInstance();
    }

    public void destroy() {
        if(realm != null) {
            Timber.d("DataManager Destroying realm Instance");
            realm.close();
        }
        m_DataMInstance = null;
    }

    public void saveSerie(String json) {
        Realm realm = Realm.getDefaultInstance();
        if(!realm.isInTransaction()) {
            realm.beginTransaction();
        }
        realm.createOrUpdateObjectFromJson(Data.class, json);
        realm.commitTransaction();
        realm.close();
    }

    public boolean serieHasData(int serieId) {
        Data serie = realm.where(Data.class).equalTo("id", serieId).findFirst();
        return serie != null && serie.getSeasons().size() > 0;
    }

    public Data getSerie(int serieId) {
        return realm.where(Data.class).equalTo("id", serieId).findFirst();
    }

    public RealmList<Seasons> getSeasonsFromSerieId(int serieId) {
        Data serie = realm.where(Data.class).equalTo("id", serieId).findFirst();
        if(serie == null)
            return null;
        return serie.getSeasons();
    }
}
